# Tuczno Museum Association Event Registration System

## Overview
This application is a frontend event registration system developed for the Tuczno Museum Association. It was created as part of a freelance project and includes features such as user registration, login, password change, password reset via email, event registration, an admin panel, event creation, and more.

## Technology Stack
The application is built using the following technologies:
- **React**: A JavaScript library for building user interfaces.
- **Redux**: A predictable state container for JavaScript apps.
- **TypeScript**: An open-source language which builds on JavaScript by adding static type definitions.
- **Formik**: A small library that helps with the 3 most annoying parts: Getting values in and out of form state, Validation and error messages, and Handling form submission.
- **Redux-Thunk**: Middleware that allows you to write action creators that return a function instead of an action.
- **Material UI**: A popular React UI framework for faster and easier web development.

## Features
- **User Registration**: New users can create an account.
- **User Login**: Registered users can log in to the system.
- **Password Change**: Users can change their password.
- **Password Reset via Email**: Users can reset their password through their registered email.
- **Event Registration**: Users can register for events.
- **Admin Panel**: Admins can manage the system and its users.
- **Event Creation**: Admins can create new events.

## Preview 
![](https://i.imgur.com/ZB3PhHm.png)
