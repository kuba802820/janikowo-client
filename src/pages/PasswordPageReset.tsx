import React from 'react';
import Navbar from '../components/Navbar/TopNav/Nav';
import SideMenu from '../components/Navbar/SideMenu/SideMenu';
import SendEmailForm from '../components/ResetPassword/SendEmail/SendEmailForm';
import PasswordReset from '../components/PasswordReset/PasswordReset';
import jwt from 'jsonwebtoken';

type Token = { 
    token: string,
    exp?: string,
}
interface IResetPasswordProps {
    match: {
        params: Token;
    };
}
const PasswordPageReset = (props: IResetPasswordProps): React.ReactElement => {
    const token = props.match.params.token;
    const tokenDecode: any = jwt.decode(props.match.params.token);
    return (
        <div>
            <Navbar />
            <SideMenu />
            <div>
                {props.match.params.token ? (
                    token !== null ? (
                        Number(tokenDecode.exp) < (new Date().getTime() + 1) / 1000 ? (
                            <SendEmailForm />
                        ) : (
                            <PasswordReset withToken={true} token={props.match.params.token} isFilledSite={true} />
                        )
                    ) : (
                        <SendEmailForm />
                    )
                ) : (
                    <SendEmailForm />
                )}
            </div>
        </div>
    );
};
export default PasswordPageReset;
