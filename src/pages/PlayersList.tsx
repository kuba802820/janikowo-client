import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import PlayerRowList from '../components/Enrolements/PlayerRowList';
import Loading from '../components/Helpers/Loading';
import SideMenu from '../components/Navbar/SideMenu/SideMenu';
import Navbar from '../components/Navbar/TopNav/Nav';
import { useData } from '../hooks/useData';
import { IAppState } from '../redux/types';

interface IPlayerList {
    match: {
        params: {
            eventId: number;
        };
    };
}

const PlayerList = (props: IPlayerList) => {
    const isAuth = useSelector((state: IAppState) => state.auth.login.isLogin);
    const res: any = useData('allresult', 'post', { eventId: +props.match.params.eventId });
    if (res) {
        const arr = Object.keys(res);
        return (
            <div>
                {isAuth ? (
                    <>
                        <Navbar />
                        <SideMenu />
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center">Id</TableCell>
                                        <TableCell align="center">Imię</TableCell>
                                        <TableCell align="center">Nazwisko</TableCell>
                                        <TableCell align="center">Miasto</TableCell>
                                        <TableCell align="center">Opłacony?</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {arr.length > 0 ? (
                                        arr.map(v => (
                                            <PlayerRowList
                                                key={res[v].id}
                                                id={res[v].id}
                                                firstName={res[v].firstName}
                                                lastName={res[v].lastName}
                                                city={res[v].city}
                                                isPay={res[v].isPay}
                                            />
                                        ))
                                    ) : (
                                        <TableRow>
                                            <TableCell align="center" component="th" scope="row"></TableCell>
                                            <TableCell align="center">
                                                <Typography variant="h5" align="center">
                                                    Brak wyników :(
                                                </Typography>
                                            </TableCell>
                                            <TableCell align="center"></TableCell>
                                        </TableRow>
                                    )}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </>
                ) : (
                    <Redirect to="/" />
                )}
            </div>
        );
    } else {
        return <Loading />;
    }
};
export default PlayerList;
