import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import Loading from '../components/Helpers/Loading';
import SideMenu from '../components/Navbar/SideMenu/SideMenu';
import Navbar from '../components/Navbar/TopNav/Nav';
import RegisterWrapper from '../components/RegisterForm/RegisterWrapper';
import { IAppState } from '../redux/types';
            
interface IAssignOtherPersonProps {
    match: {
        params: {
            eventId: string;
        };
    };
}

const AssignOtherPerson = (props: IAssignOtherPersonProps) => {
    const isAuth = useSelector((state: IAppState) => state.auth.login.isLogin);
    return (
        <div>
            {isAuth ? (
                <>
                    <Navbar />
                    <SideMenu />
                    {props ? (
                        <RegisterWrapper isAssignOtherPerson={true} eventId={props.match.params.eventId} />
                    ) : (
                        <Loading />
                    )}
                </>
            ) : (
                <Redirect to="/" />
            )}
        </div>
    );
};
export default AssignOtherPerson;
