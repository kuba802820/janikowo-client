import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import AccountRemove from '../components/AccountRemove/AccountRemove';
import Loading from '../components/Helpers/Loading';
import SideMenu from '../components/Navbar/SideMenu/SideMenu';
import Navbar from '../components/Navbar/TopNav/Nav';
import PasswordReset from '../components/PasswordReset/PasswordReset';
import ProfileItem from '../components/ProfileItem/ProfileItem';
import { useData } from '../hooks/useData';
import { IAppState } from '../redux/types';

const Profile = () => {
    interface IResponseData {
        result: {
            firstName: string;
            lastName: string;
            email: string;
            dateBrith: string;
            city: string;
            telNumber: number;
            club: string;
            shirtSize: string;
            gender: string;
        };
    }
    const isAuth = useSelector((state: IAppState) => state.auth.login.isLogin);
    const UserData = useData<IResponseData>('me', 'get');
    return (
        <div>
            {isAuth ? (
                <>
                    <Navbar />
                    <SideMenu />
                    {!!UserData ? (
                        <>
                            <ProfileItem data={UserData} />
                            <PasswordReset withToken={false} isFilledSite={false} />
                            <AccountRemove />
                        </>
                    ) : (
                        <Loading />
                    )}
                </>
            ) : (
                <Redirect to="/" />
            )}
        </div>
    );
};

export default Profile;
