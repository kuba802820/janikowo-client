import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import EnrolmentsWrapper from '../components/Enrolements/EnrolmentsWrapper';
import SideMenu from '../components/Navbar/SideMenu/SideMenu';
import Navbar from '../components/Navbar/TopNav/Nav';
import { getAssignEventList } from '../redux/Enrolments/actions.enrolments';
import { IAppState } from '../redux/types';

const Enrolments = (): React.ReactElement => {
    const dispatch = useDispatch();
    const isAuth = useSelector((state: IAppState) => state.auth.login.isLogin);
    useEffect(() => {
        dispatch(getAssignEventList());
    }, [dispatch]);
    const EventsData = useSelector((state: IAppState) => state.Enrolments.enrolments);
    return (
        <div>
            {isAuth ? (
                <>
                    <Navbar />
                    <SideMenu />
                    <EnrolmentsWrapper {...EventsData} />
                </>
            ) : (
                <Redirect to="/" />
            )}
        </div>
    );
};
export default Enrolments;
