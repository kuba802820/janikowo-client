import React from 'react';
import Navbar from '../../components/Navbar/TopNav/Nav';
import SideMenu from '../../components/Navbar/SideMenu/SideMenu';
import { Redirect } from 'react-router';
import { useSelector } from 'react-redux';
import { IAppState } from '../../redux/types';
import CreateEvents from '../../components/Admin/Components/CreateEvents/CreateEvents';
import ListEvents from '../../components/Admin/Components/ListEvents/ListEvents';

const Admin = () => {
    const isAdmin = useSelector((state: IAppState) => state.auth.login.isAdmin);
    return (
        <div>
            {isAdmin ? (
                <>
                    <Navbar />
                    <SideMenu />
                    <CreateEvents />
                    <ListEvents />
                </>
            ) : (
                <Redirect to="/apply" />
            )}
        </div>
    );
};

export default Admin;
