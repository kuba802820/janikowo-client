import React from 'react';
import Navbar from '../../components/Navbar/TopNav/Nav';
import SideMenu from '../../components/Navbar/SideMenu/SideMenu';
import { Redirect } from 'react-router';
import { useSelector } from 'react-redux';
import { IAppState } from '../../redux/types';
import MarkPayList from '../../components/Admin/Components/MarkPayList/MarkPayList';

interface IMarkPayProps {
    match: {
        params: {
            eventId: string;
        };
    };
}

const MarkPay = (props: IMarkPayProps) => {
    const isAdmin = useSelector((state: IAppState) => state.auth.login.isAdmin);
    const eventId = props.match.params.eventId;
    return (
        <div>
            {isAdmin ? (
                <>
                    <Navbar />
                    <SideMenu />
                    <MarkPayList eventId={eventId} />
                </>
            ) : (
                <Redirect to="/apply" />
            )}
        </div>
    );
};

export default MarkPay;
