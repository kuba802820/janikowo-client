const titleItems = {
    firstName: 'Imie',
    lastName: 'Nazwisko',
    email: ' Email',
    dateBrith: 'Data ur.',
    city: 'Miasto',
    telNumber: 'Nr.Telefonu',
    club: 'Klub',
    shirtSize: 'Rozmiar Koszulki',
    gender: 'Płeć',
};
type ArrayOfKeys = {
    [key: string]: string,
}
type ReturnType = {
    titleItems: ArrayOfKeys,
    keysToDisplay: string[],
}
export const useProfileItem = (data: object, itemToHide: string[]): ReturnType => {
    const keys = Object.keys(data);
    const keysToDisplay = keys.filter(el => !itemToHide.includes(el));
    return { titleItems, keysToDisplay };
};
