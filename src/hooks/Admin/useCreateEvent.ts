/* eslint-disable @typescript-eslint/no-unused-vars */
import { useDispatch } from 'react-redux';
import { makeRequest } from '../../API/reqAPI';
import { displayInfo } from '../../API/formNotification';
import { getAssignEventList } from '../../redux/Enrolments/actions.enrolments';

export const useCreateEvent = () => {
    const dispatch = useDispatch();
    interface ICreateEventResponse {
        eventName: string,
        rulesLink: string,
    }
    return {
        createEvent: async ({ eventName, rulesLink }: { eventName: string; rulesLink: string }) => {
            const res = await makeRequest<string, ICreateEventResponse>('admin/createEvent', 'post', { eventName, rulesLink });
            const isError = res?.error ? true : false;
            !isError ? displayInfo(res?.data.result, 5000, dispatch) : displayInfo(res?.error, 5000, dispatch);
            dispatch(getAssignEventList());
        },
    };
};
