/* eslint-disable @typescript-eslint/no-unused-vars */
import { useDispatch } from 'react-redux';
import { makeRequest } from '../../API/reqAPI';
import { getAssignEventList } from '../../redux/Enrolments/actions.enrolments';

export const useActionEvent = () => {
    const dispatch = useDispatch();
    return {
        removeEvent: async ({ eventId }: { eventId: number }) => {
            await makeRequest(`admin/removeEvent?id=${eventId}`, 'get');
            dispatch(getAssignEventList());
        },
        toggleAssignEvent: async ({ eventId, isActive }: { eventId: number, isActive: number }) => {
            await makeRequest(`admin/setEventState?id=${eventId}&isActive=${isActive}`, 'get');
            dispatch(getAssignEventList());
        }
    };
};
