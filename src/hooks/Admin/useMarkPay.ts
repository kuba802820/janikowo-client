/* eslint-disable @typescript-eslint/no-unused-vars */
import { makeRequest } from '../../API/reqAPI';

export const useMarkPay = () => {
    return {
        markPay: async ({ eventId, isPay }: { eventId: number; isPay: number }) =>
            await makeRequest('admin/markPay?id=' + eventId + '&isPay=' + isPay, 'get'),
    };
};
