import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { makeRequest, Method } from '../API/reqAPI';
import { logoutAction } from '../redux/Auth/action.auth';
export const useData = <T>(endpoint: string, method: Method, data?: T) => {
    const [Data, setData] = useState();
    const dispatch = useDispatch();
    useEffect(() => {
        const getData = async () => {
            try {
                const res: any = await makeRequest<T>(endpoint, method, data);
                setData(res.data.result);
            } catch (error) {
                dispatch(logoutAction());
            }
        };
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return Data;
};


