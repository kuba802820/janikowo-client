import { useDispatch } from 'react-redux';
import { displayInfo } from '../API/formNotification';
import { makeRequest } from '../API/reqAPI';

interface IPasswordChange {
    values: {
        actualPassword?: string;
        confirmPassword: string;
        newPassword: string;
    };
}

export const usePasswordChange = () => {
    const dispatch = useDispatch();
    return {
        changePassword: async ({ values }: IPasswordChange, token?: string) => {
            const { actualPassword, confirmPassword, newPassword } = values;
            const isToken = !!token ? token : 'null';
            const req = await makeRequest(
                'resetpassword',
                'post',
                { actualPassword, confirmPassword, newPassword },
                { Authorization: `Bearer ${isToken}` },
            );
            const isError = req?.error ? true : false;
            if (!isError) {
                displayInfo('Hasło zmienione, możesz się zalogować', 5000, dispatch);
            } else {
                displayInfo(req?.error, 5000, dispatch);
            }
        },
    };
};
