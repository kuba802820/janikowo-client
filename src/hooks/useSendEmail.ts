import { makeRequest } from '../API/reqAPI';
import { useDispatch } from 'react-redux';
import { displayInfo } from '../API/formNotification';

export const useSendEmail = () => {
    const dispatch = useDispatch();
    return {
        sendResetEmail: async ({ email }: { email: string }): Promise<void> => {
            await makeRequest('sendresetemail', 'post', { email });
            displayInfo(
                'Jeśli masz konto w naszej aplikacji to wysłaliśmy ci email z dalszym postępowaniem',
                2000,
                dispatch,
            );
        },
    };
};
