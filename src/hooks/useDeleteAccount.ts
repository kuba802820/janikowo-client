import { makeRequest } from '../API/reqAPI';
import { useDispatch } from 'react-redux';
import { LogoutUser } from '../redux/Auth/action.auth';
import { useState } from 'react';

export const useDeleteAccount = () => {
    const dispatch = useDispatch();
    const [error, setError] = useState('');
    return {
        deleteAccount: async ({ email, password }: { email: string; password: string }) => {
            const req = await makeRequest('removeaccount', 'post', { email, password });
            const isError = req?.error ? true : false;
            !isError ? dispatch(LogoutUser()) : setError(req?.error);
        },
        error,
    };
};
