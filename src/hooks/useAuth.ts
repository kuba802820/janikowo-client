import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { makeRequest } from '../API/reqAPI';
import { LogoutUser } from '../redux/Auth/action.auth';
interface CheckLoginRes {
    result: boolean
}
export const useAuth = () => {
    const [isAuth, setIsAuth] = useState(false);
    const dispatch = useDispatch();
    const checkAuthOnServer = async () => {
        const {data} = await makeRequest<CheckLoginRes>('checklogin', 'get');
        if(data.result && typeof data.result === 'boolean'){
            setIsAuth(data.result);
        }
    }
    return {
        checkAuth: () => {
            checkAuthOnServer();
            if(!isAuth) dispatch(LogoutUser());
            return isAuth;
        }

    }
};
