import { EventData } from '../components/Enrolements/types';

/* eslint-disable @typescript-eslint/interface-name-prefix */
export type ILogin = {
    email: string;
    role?: string;
    club?: string;
    password?: string;
    userId?: string;
    isLogin?: boolean;
    isAdmin?: boolean;
    loginErrorMessage?: string;
};
export type IAssignEventList = {
    alreadyEnrolments?: string[];
    eventsData: EventData;
};

export type IDrawerMenu = {
    isOpen: boolean;
};
export type IErrors = {
    formTopError: string;
};

export interface IAppState {
    auth: {
        login: ILogin;
    };
    MobileMenu: {
        drawer: IDrawerMenu;
    };
    errors: {
        errors: IErrors;
    };
    Enrolments: {
        enrolments: IAssignEventList;
    };
}
