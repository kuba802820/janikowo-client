export const TOGGLE_DRAWER_MOBILE_MENU = 'TOGGLE_DRAWER_MOBILE_MENU';

export interface IToogleDrawer {
    type: typeof TOGGLE_DRAWER_MOBILE_MENU;
    payload: {
        isOpen: boolean;
    };
}
