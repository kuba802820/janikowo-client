import * as Yup from 'yup';
export const SigninSchema = Yup.object().shape({
    email: Yup.string()
        .email('Nieprawidłowy adres email')
        .required('Pole wymagane'),
    password: Yup.string().required('Pole wymagane'),
});
