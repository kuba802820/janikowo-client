import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles({
    root: {
        display: 'flex',
        justifyContent: 'center',
    },
    deleteBtn: {
        margin: 10,
    },
});
