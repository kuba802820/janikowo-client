import React from 'react';
import { Typography } from '@material-ui/core';
import { useStyle } from './AccountRemove.style';
import FormDialog from './FormAccountRemove';

const AccountRemove = () => {
    const classes = useStyle();
    return (
        <>
            <Typography color="textPrimary" align="center" variant="h3" component="h2">
                Usuń Konto
            </Typography>
            <div className={classes.root}>
                <FormDialog />
            </div>
        </>
    );
};
export default AccountRemove;
