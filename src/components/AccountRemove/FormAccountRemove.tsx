import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useDeleteAccount } from '../../hooks/useDeleteAccount';
import { useSelector } from 'react-redux';
import { IAppState } from '../../redux/types';
import { useStyle } from './AccountRemove.style';

const FormDialog = () => {
    const { error, deleteAccount } = useDeleteAccount();
    const [open, setOpen] = useState(false);
    const [password, setPassword] = useState('');
    const email = useSelector((state: IAppState) => state.auth.login.email);
    // const error = useSelector((err: IAppState) => err.errors.errors.formTopError);
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const classes = useStyle();
    const handleSubmit = () => {
        deleteAccount({ password, email });
    };

    return (
        <div>
            <Button variant="contained" color="primary" className={classes.deleteBtn} onClick={handleClickOpen}>
                Usuń Konto
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Chcesz usunąć konto? Na pewno?</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Gdy wpiszesz poniżej swoje poprawne hasło zostaniesz automatycznie wylogowany, a twoje konto
                        zostanie usunięte na zawsze!
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Hasło"
                        type="password"
                        error={!!error}
                        helperText={error}
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Anuluj
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Usuń Konto
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};
export default FormDialog;
