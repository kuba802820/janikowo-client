import { TableCell, TableRow } from '@material-ui/core';
import React from 'react';

type PlayerRowData = {
    id: number, 
    firstName: string,
    lastName: string,
    city: string,
    isPay: number,
}

const PlayerRowList = ({ id, firstName, lastName, city, isPay }: PlayerRowData) => {
    return (
        <TableRow>
            <TableCell align="center" size="medium" component="th" scope="row">
                <b>{id}</b>
            </TableCell>
            <TableCell align="center" size="medium" component="th" scope="row">
                {firstName}
            </TableCell>
            <TableCell align="center" size="medium">
                {lastName}
            </TableCell>
            <TableCell align="center" size="medium">
                {city}
            </TableCell>
            <TableCell align="center" size="medium">
                <b>{isPay ? "TAK" : "NIE"}</b>
            </TableCell>
        </TableRow>
    );
};

export default PlayerRowList;
