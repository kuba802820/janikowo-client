/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Button, TableCell, TableRow } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useEnroll } from '../../hooks/useEnroll';
import { useStyle } from './Enrolments.style';
import { IEvent } from './types';




const EventRow = ({ data, alreadyEnrolments }: IEvent) => {
    const [isEnroll, setEnroll] = useState(false);
    const history = useHistory();
    const classes = useStyle();
    const checkEnroll = ({ data, alreadyEnrolments }: IEvent) => {
        if(alreadyEnrolments) setEnroll(alreadyEnrolments.includes(data.id) ? true : false);
    }
    useEffect(() => {
       checkEnroll({ data, alreadyEnrolments });
    }, [data, alreadyEnrolments])
    const { assignEvent } = useEnroll();
    return (
        <TableRow key={data.id}>
            <TableCell align="center" component="th" scope="row">
                {data.eventName}
            </TableCell>
            <TableCell align="center">{data.joinedUser}</TableCell>
            <TableCell align="center">
                {data.isActive ? 
                <Button
                    variant="contained"
                    color="primary"
                    disabled={isEnroll}
                    onClick={async () => {
                        
                        const id = data.id.toString() || undefined;
                        assignEvent({ eventId: id, isAssignOtherPerson: false });
                    }}
                >
                    {isEnroll ? 'Zapisano' : 'Zapisz się'}
                </Button>
                : <b>Zapisy Zamknięte</b>}
            </TableCell>
            <TableCell align="center">
                {data.isActive ? 
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        history.push('assignOtherPerson/' + data.id);
                    }}
                >
                    Zapisz kogoś
                </Button>
                : <b>Zapisy Zamknięte</b>
                }
            </TableCell>
            <TableCell align="center">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        history.push('playerlist/' + data.id);
                    }}
                >
                    Lista zawodników
                </Button>
            </TableCell>
            <TableCell align="center">
                <a href={data.rulesLink} className={classes.linkStyle} target="_blank" rel="noopener noreferrer">
                    <Button variant="contained" color="primary">
                        Regulamin
                    </Button>
                </a>
            </TableCell>
        </TableRow>
    );
};
export default EventRow;
