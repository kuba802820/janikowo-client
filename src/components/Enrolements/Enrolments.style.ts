import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles(() => ({
    root: {
        height: '90vh',
        width: '100%',
        padding: '10 0px',
    },
    linkStyle: {
        textDecoration: 'none',
    },
}));
