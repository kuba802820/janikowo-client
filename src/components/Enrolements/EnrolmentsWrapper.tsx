import React from 'react';
import EventRow from './EventRow';
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { useStyle } from './Enrolments.style';
import { EventData } from './types';
import Loading from '../Helpers/Loading';

const EnrolmentsWrapper = ({
    eventsData,
    alreadyEnrolments,
}: {
    eventsData: EventData;
    alreadyEnrolments?: string[];
}) => {
    const classes = useStyle();
    if (eventsData) {
        const arr = Object.keys(eventsData);
        return (
            <div className={classes.root}>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="center">Nazwa wydarzenia</TableCell>
                                <TableCell align="center">L. zap. ucz</TableCell>
                                <TableCell align="center">Zapisz się</TableCell>
                                <TableCell align="center">Zapisz kogoś</TableCell>
                                <TableCell align="center">Lista uczestników</TableCell>
                                <TableCell align="center">Regulamin zawodów</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {arr.map(v => (
                                <EventRow
                                    key={eventsData[v].id}
                                    data={eventsData[v]}
                                    alreadyEnrolments={alreadyEnrolments}
                                />
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        );
    } else {
        return <Loading />;
    }
};
export default EnrolmentsWrapper;
