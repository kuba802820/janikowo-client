export interface IEvent {
    data: {
        id: string;
        eventName: string;
        joinedUser: string;
        rulesLink: string;
        isActive: boolean;
    };
    alreadyEnrolments?: string[];
}
export type ArrayEventData = {
    id: string;
    eventName: string;
    rulesLink: string;
    joinedUser: string;
    isActive: boolean;
};

export type EventData = {
    [index: string]: ArrayEventData;
};
