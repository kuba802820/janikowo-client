import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Button, TextField, Typography } from '@material-ui/core';
import { useStyle } from './SendEmailForm.style';
import { useSendEmail } from '../../../hooks/useSendEmail';
import { useSelector } from 'react-redux';
import { IAppState } from '../../../redux/types';
const SendEmailForm = () => {
    const classes = useStyle();
    const emailResetValidation = Yup.object().shape({
        email: Yup.string()
            .email('Nieprawidłowy adres email')
            .required('Pole wymagane'),
    });
    const { sendResetEmail } = useSendEmail();
    const error = useSelector((err: IAppState) => err.errors.errors.formTopError);
    return (
        <div className={classes.root}>
            <Formik
                initialValues={{
                    email: '',
                }}
                validationSchema={emailResetValidation}
                onSubmit={values => {
                    sendResetEmail(values);
                }}
            >
                {({ errors, handleChange, handleBlur, handleSubmit }) => (
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <Typography variant="h5" align="center" component="p">
                            Zresetuj zapomniane hasło.
                        </Typography>
                        {!!error && (
                            <Typography className={classes.errorLabel} align="center" variant="body1" component="p">
                                {error}
                            </Typography>
                        )}
                        <TextField
                            error={!!errors.email ? true : false}
                            className={classes.formItems}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="email"
                            label="Email"
                            helperText={errors.email}
                            variant="filled"
                        />
                        <Button type="submit" variant="contained" className={classes.formItems} color="primary">
                            Zresetuj hasło
                        </Button>
                    </form>
                )}
            </Formik>
        </div>
    );
};
export default SendEmailForm;
