import React, { useState, useEffect } from 'react';
import { useStyle } from './CokkieAlert.style';
import { Typography } from '@material-ui/core';

const CokkieAlert = () => {
    const classes = useStyle();
    const [isAlertShown, setShowAlert] = useState(true);
    useEffect(() => {
        const isEverShown = localStorage.getItem('cookieAlert');
        isEverShown ? setShowAlert(false) : setShowAlert(true);
    }, []);
    const handleClose = (isApproved: boolean) => {
        setShowAlert(false);
        localStorage.setItem('cookieAlert', `{"show": false, "isApproved": ${isApproved}}`);
    };
    return (
        <div>
            {isAlertShown ? (
                <div className={classes.root}>
                    <div className={classes.cookieContent}>
                        <Typography variant="body2" component="p">
                            Ta strona wykorzystuje pliki cookie Używamy informacji zapisanych za pomocą plików cookies w
                            celu zapewnienia maksymalnej wygody w korzystaniu z naszego serwisu. Jeżeli wyrażasz zgodę na
                            zapisywanie informacji zawartej w cookies kliknij na „Zamknij” u dołu tej informacji. Jeśli
                            nie wyrażasz zgody, ustawienia dotyczące plików cookies możesz zmienić w swojej
                            przeglądarce.
                        </Typography>
                        <div className={classes.buttonWrapper}>
                            <div className={classes.closeCookieApproved}  onClick={() => handleClose(true)}>
                                <Typography variant="body2" align="center" component="p">
                                    Wyrażam zgodę
                                </Typography>
                            </div>
                            <div className={classes.closeCookieDecline} onClick={() => handleClose(false)}>
                                <Typography variant="body2" align="center" component="p">
                                    Nie wyrażam zgody
                                </Typography>
                            </div>
                        </div>
                       
                    </div>
                </div>
            ) : null}
        </div>
    );
};
export default CokkieAlert;
