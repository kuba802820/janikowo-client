import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles(() => ({
    root: {
        height: 'auto',
        width: '100%',
        padding: '10px',
        background: 'rgba(30, 38, 46,0.9)',
        position: 'fixed',
        bottom: 0,
        left: 0,
    },
    cookieContent: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'column',
        color: '#fff',
    },
    buttonWrapper: {
        width: '100%',
        display: 'flex',
    },
    closeCookieDecline: {
        marginTop: '10px',
        height: '100%',
        width: '50%',
        padding: '10px',
        background: '#f04d4d',
        transition: 'background 200ms',
        '&:hover': {
            background: "#f55858",
            cursor: 'pointer',
        },
    },
    closeCookieApproved: {
        marginTop: '10px',
        height: '100%',
        width: '50%',
        padding: '10px',
        background: '#008000',
        transition: 'background 200ms',
        '&:hover': {
            background: "#3ec762",
            cursor: 'pointer',
        },
    },
}));
