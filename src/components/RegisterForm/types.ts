/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

export interface IRegisterFormContext {
    errors?: any;
    values?: any;
    handleChange?: (
        eventOrPath: string | React.ChangeEvent<any>,
    ) => void | ((eventOrTextValue: string | React.ChangeEvent<any>) => void);
    handleBlur?: (eventOrString: any) => void | ((e: any) => void);
    handleSubmit?: (e?: React.FormEvent<HTMLFormElement>) => void;
}
export interface IButtonsProps {
    actualStep: number;
    maxSteps: number;
    handleStepBackward: ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined;
    handleStepForward: ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined;
    classes: any;
}
