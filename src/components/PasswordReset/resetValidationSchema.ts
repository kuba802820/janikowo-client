import * as Yup from 'yup';
export const ResetSchemaWithoutToken = Yup.object().shape({
    actualPassword: Yup.string().required('Pole wymagane'),
    newPassword: Yup.string()
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/, 'Hasło musi zawierać jedną dużą literę, jedną małą literę, oraz cyfrę')
        .min(8, 'Hasło musi mieć minimum 8 znaków')
        .required('Pole wymagane'),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('newPassword'), null], 'Hasła się nie zgadzają.')
        .required('Pole wymagane'),
});
export const ResetSchemaWithToken = Yup.object().shape({
    newPassword: Yup.string()
        .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/, 'Hasło musi zawierać jedną dużą literę, oraz cyfrę')
        .min(8, 'Hasło musi mieć minimum 8 znaków')
        .required('Pole wymagane'),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('newPassword'), null], 'Hasła się nie zgadzają.')
        .required('Pole wymagane'),
});
