import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles({
    root: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        height: (props: { isFilledSite: boolean; }) => props.isFilledSite ? '90vh' : 'auto',
    },
    formResetPassword: {
        display: 'flex',

        padding: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        '@media screen and (min-width: 320px)': {
            width: '100%',
        },
        '@media screen and (min-width: 1024px)': {
            width: '40%',
        },
        '@media screen and (min-width: 2560px)': {
            width: '20%',
        },
    },
    resetPasswordInput: {
        marginTop: 10,
    },
    errorLabel: {
        color: '#ed6258',
    },
});
