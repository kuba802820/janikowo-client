import React, { useEffect } from 'react';
import { Typography, TextField, Button } from '@material-ui/core';
import { useStyle } from './PasswordReset.style';
import { usePasswordChange } from '../../hooks/usePasswordReset';
import { Formik } from 'formik';
import { ResetSchemaWithoutToken, ResetSchemaWithToken } from './resetValidationSchema';
import { useDispatch, useSelector } from 'react-redux';
import { IAppState } from '../../redux/types';
import { clearErrors } from '../../redux/Errors/action.errors';
const PasswordReset = ({
    withToken,
    token,
    isFilledSite,
}: {
    withToken: boolean;
    token?: string;
    isFilledSite: boolean;
}) => {
    const dispatch = useDispatch();
    const { changePassword } = usePasswordChange();
    const classes = useStyle({ isFilledSite });
    const error = useSelector((err: IAppState) => err.errors.errors.formTopError);
    useEffect(() => {
        dispatch(clearErrors());
    }, [dispatch]);
    const isWithToken = [{actualPassword: '' , newPassword: '', confirmPassword: ''}, {newPassword: '',confirmPassword: ''}];
    return (
        <>
            <div className={classes.root}>
                <Typography color="textPrimary" align="center" variant="h3" component="h2">
                    Zmień hasło
                </Typography>
                <Formik
                    initialValues={withToken ? isWithToken[1] : isWithToken[0]}
                    validationSchema={withToken ? ResetSchemaWithToken : ResetSchemaWithoutToken}
                    onSubmit={values => {
                        changePassword({ values }, token);
                    }}
                >
                    {({ errors, handleChange, handleBlur, handleSubmit }) => (
                        <form className={classes.formResetPassword} onSubmit={handleSubmit}>
                            {!!error && (
                                <Typography className={classes.errorLabel} variant="body1" component="p">
                                    {error}
                                </Typography>
                            )}
                            {!withToken && (
                                <TextField
                                    error={!!errors.actualPassword ? true : false}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={classes.resetPasswordInput}
                                    name="actualPassword"
                                    label="Aktualne Hasło"
                                    type="password"
                                    helperText={errors.actualPassword}
                                    variant="filled"
                                />
                            )}

                            <TextField
                                error={!!errors.newPassword ? true : false}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                name="newPassword"
                                label="Nowe hasło"
                                type="password"
                                className={classes.resetPasswordInput}
                                helperText={errors.newPassword}
                                variant="filled"
                            />
                            <TextField
                                error={!!errors.confirmPassword ? true : false}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                name="confirmPassword"
                                label="Powtórz nowe hasło"
                                type="password"
                                className={classes.resetPasswordInput}
                                helperText={errors.confirmPassword}
                                variant="filled"
                            />
                            <Button
                                type="submit"
                                variant="contained"
                                className={classes.resetPasswordInput}
                                color="primary"
                            >
                                Zmień hasło
                            </Button>
                        </form>
                    )}
                </Formik>
            </div>
        </>
    );
};
export default PasswordReset;
