import { Dispatch } from 'redux';

export interface IPropsNav {
    data: {
        isOpen: boolean;
        handleToggle: void;
        handleLogout: void;
    };
}

export interface IPropsNavItems {
    link: string;
    isAdmin: boolean;
    alwaysVisible: boolean;
    text: string;
    action?: {
        logout: (dispatch: Dispatch<any>) => void;
    };
}
