import React from 'react';
import { TextField, Button, Typography } from '@material-ui/core';
import { useStyle } from './CreateEvents.style';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useCreateEvent } from '../../../../hooks/Admin/useCreateEvent';
import { useSelector } from 'react-redux';
import { IAppState } from '../../../../redux/types';

const EventsSchema = Yup.object().shape({
    eventName: Yup.string().required('Pole wymagane'),
    rulesLink: Yup.string().required('Pole wymagane'),
});

const CreateEvents = () => {
    const { createEvent } = useCreateEvent();
    const error = useSelector((err: IAppState) => err.errors.errors.formTopError);
    const classes = useStyle();
    return (
        <Formik
            initialValues={{
                eventName: '',
                rulesLink: '',
            }}
            validationSchema={EventsSchema}
            onSubmit={({ eventName, rulesLink }) => {
                createEvent({ eventName, rulesLink });
            }}
        >
            {({ errors, handleChange, handleBlur, handleSubmit }) => (
                <form className={classes.root} onSubmit={handleSubmit}>
                    {!!error && (
                        <Typography className={classes.errorLabel} variant="body1" component="p">
                            {error}
                        </Typography>
                    )}
                    <TextField
                        name="eventName"
                        error={!!errors.eventName ? true : false}
                        helperText={errors.eventName}
                        className={classes.formItem}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        label="Nazwa Wydarzenia"
                        variant="filled"
                    />
                    <TextField
                        name="rulesLink"
                        error={!!errors.rulesLink ? true : false}
                        helperText={errors.rulesLink}
                        className={classes.formItem}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        label="Link do regulaminu"
                        variant="filled"
                    />
                    <Button type="submit" className={classes.formItem} variant="contained" color="primary">
                        Utwórz Wydarzenie
                    </Button>
                </form>
            )}
        </Formik>
    );
};
export default CreateEvents;
