import { makeStyles } from '@material-ui/styles';

export const useStyle = makeStyles(() => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    formItem: {
        margin: 10,
        '@media screen and (min-width: 414px)': {
            width: '50%',
        },
        '@media screen and (min-width: 768px)': {
            width: '30%',
        },
        '@media screen and (min-width: 1200px)': {
            width: '20%',
        },
    },
    errorLabel: {
        color: '#ed6258',
    },
}));
