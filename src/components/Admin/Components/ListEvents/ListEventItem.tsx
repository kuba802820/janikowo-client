import { TableRow, TableCell, Button } from '@material-ui/core';
import React from 'react';
import { useActionEvent } from '../../../../hooks/Admin/useActionEvent';
import { useHistory } from 'react-router';

interface ListEventItem {
    data: {
        id: string;
        eventName: string;
        isActive: boolean;
    };
};

const ListEventItem = ({ data }: ListEventItem) => {
    const { removeEvent, toggleAssignEvent } = useActionEvent();
    const history = useHistory();
    return (
        <TableRow>
            <TableCell align="center" component="th" scope="row">
                {data.eventName}
            </TableCell>
            <TableCell align="center">
                <Button variant="contained" color="primary" onClick={() => history.push('markpay/' + data.id)}>
                    Oznaczanie opłaconych
                </Button>
            </TableCell>
            <TableCell align="center">
                <Button variant="contained" color="primary" onClick={() => removeEvent({ eventId: Number(data.id) })}>
                    USUŃ ID: [{data.id}]
                </Button>
            </TableCell>
            <TableCell align="center">
                <Button variant="contained" color="primary" onClick={() => toggleAssignEvent({ eventId: Number(data.id), isActive: Number(!data.isActive) })}>
                    {data.isActive ? "Wyłącz" : "Włącz"}
                </Button>
            </TableCell>
        </TableRow>
    );
};
export default ListEventItem;
