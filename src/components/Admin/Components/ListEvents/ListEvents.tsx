import React from 'react';
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import ListEventItem from './ListEventItem';
import { useSelector } from 'react-redux';
import { IAppState } from '../../../../redux/types';

const ListEvents = () => {
    const { eventsData } = useSelector((state: IAppState) => state.Enrolments.enrolments);
    const arr = Object.keys(eventsData);
    return (
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">Nazwa wydarzenia</TableCell>
                        <TableCell align="center">Ozn. Opłaconych</TableCell>
                        <TableCell align="center">Usuń</TableCell>
                        <TableCell align="center">Status Zapisów</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {arr.map(v => (
                        <ListEventItem
                            key={eventsData[v].id}
                            data={{ id: eventsData[v].id, eventName: eventsData[v].eventName, isActive: eventsData[v].isActive  }}
                        />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};
export default ListEvents;
