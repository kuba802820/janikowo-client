import React from 'react';
import { useData } from '../../../../hooks/useData';
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import MarkPayItem from './MarkPayitem';

const MarkPayList = ({ eventId }: { eventId: string }) => {
    const data: any = useData(`admin/allResult?eventId=` + eventId, 'get');
    return (
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center">ID</TableCell>
                        <TableCell align="center">Imie I Nazwisko</TableCell>
                        <TableCell align="center">Telefon</TableCell>
                        <TableCell align="center">E-Mail</TableCell>
                        <TableCell align="center">Data Zapisania</TableCell>
                        <TableCell align="center">Miasto</TableCell>
                        <TableCell align="center">Zapisany przez</TableCell>
                        <TableCell align="center">Klub</TableCell>
                        <TableCell align="center">Zapłacone?</TableCell>
                        <TableCell align="center">Zapisz</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data &&
                        Object.keys(data).map(v => {
                            return <MarkPayItem data={data[v]} key={data[v].id} />;
                        })}
                </TableBody>
            </Table>
        </TableContainer>
    );
};
export default MarkPayList;
