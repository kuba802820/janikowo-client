import { TableRow, TableCell, Button, Checkbox } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { useMarkPay } from '../../../../hooks/Admin/useMarkPay';
type MarkPayItem = {
    data: {
        id: number;
        firstName: string;
        lastName: string;
        telNumber: number;
        email: string;
        assignBy: string;
        assignDate: string;
        city: string;
        club: string;
        isPay: boolean;
    };
};

const MarkPayItem = ({ data }: MarkPayItem) => {
    const [isPayed, setPay] = useState(0);
    const { markPay } = useMarkPay();
    useEffect(() => {
        setPay(data.isPay ? 1 : 0);
    }, [data.isPay, setPay]);
    const handleChange = (e: { target: { checked: any; }; }) => {
        setPay(e.target.checked ? 1 : 0);
    };
    return (
        <TableRow>
            <TableCell align="center" component="th" scope="row">{data.id}</TableCell>
            <TableCell align="center">{data.firstName} {data.lastName}</TableCell>
            <TableCell align="center">{data.telNumber}</TableCell>
            <TableCell align="center" scope="row">{data.email}</TableCell>
            <TableCell align="center">{data.assignDate}</TableCell>
            <TableCell align="center">{data.city}</TableCell>
            <TableCell align="center">{data.assignBy}</TableCell>
            <TableCell align="center" scope="row">{data.club}</TableCell>
            <TableCell align="center">
                <Checkbox
                    checked={Boolean(isPayed)}
                    onChange={handleChange}
                    color="primary"
                    value={isPayed}
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                />
            </TableCell>
            <TableCell align="center">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        markPay({ eventId: data.id, isPay: isPayed });
                    }}
                >
                    Zapisz
                </Button>
            </TableCell>
        </TableRow>
    );
};
export default MarkPayItem;
