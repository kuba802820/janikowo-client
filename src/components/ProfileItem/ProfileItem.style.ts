import { makeStyles } from '@material-ui/styles';
export const useStyle = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        '@media screen and (max-width: 768px)': {
            flexDirection: 'column',
        },
    },
    itemProfile: {
        flexShrink: 0,
        flexBasis: '32%',
        margin: 5,
        backgroundColor: (props: { color: string }) => props.color,
        height: '50px',
    },
    textItem: {
        color: '#fff',
    },
    titleProfile: {
        margin: 10,
    },
}));
