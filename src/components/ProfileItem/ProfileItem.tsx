import { useStyle } from './ProfileItem.style';
import { Typography, Card, CardContent } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import React from 'react';
import { useProfileItem } from '../../hooks/useProfileItems';
import Loading from '../Helpers/Loading';
const ProfileItem = ({ data }: any) => {
    const theme = useTheme();
    const classes = useStyle({color: `${theme.palette.primary.main}`});
    const { keysToDisplay, titleItems } = useProfileItem(data, ['role','createdAt']);
    return (
        <div>
            {data ? 
                <>
                    <Typography variant="h3" align="center" component="h2" className={classes.titleProfile}>
                        Profil {data?.firstName} {data?.lastName}
                    </Typography>
                    <div className={classes.root}>
                        {keysToDisplay.map(v => {
                            return (
                                
                                <Card key={v} className={classes.itemProfile}>
                                    <CardContent>
                                        <Typography color="textPrimary" className={classes.textItem} gutterBottom>
                                            {titleItems[v]}: {data[v]}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            );
                        })}
                    </div>
                </>
            : <Loading />}
            
        </div>
    );
};

export default ProfileItem;
