import { Dispatch } from 'redux';
import { setFormTopError, clearErrors } from '../redux/Errors/action.errors';

export const displayInfo = (value: any, timeToHide: number, dispatch: Dispatch) => {
    dispatch(setFormTopError({ formTopError: value }));
    setTimeout(() => {
        dispatch(clearErrors());
    }, timeToHide);
};
